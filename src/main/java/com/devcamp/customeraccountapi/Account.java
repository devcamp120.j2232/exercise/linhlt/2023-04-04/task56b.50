package com.devcamp.customeraccountapi;
import java.util.Locale;
import java.text.NumberFormat;

public class Account {
    private int id;
    private Customer customer;
    private double balance = 0.0;
    public Account(int id, Customer customer, double balance) {
        this.id = id;
        this.customer = customer;
        this.balance = balance;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public Customer getCustomer() {
        return customer;
    }
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    public double getBalance() {
        return balance;
    }
    public void setBalance(double balance) {
        this.balance = balance;
    }
    public String getCustomerName(){
        return customer.getName();
    }
    public double deposit(double amount){
        return balance + amount;
    }
    public double withdraw(double amount){
        if (balance >= amount ){
            balance = balance - amount;
        }
        else System.out.println("Amount withdraw exceeds the current balance");
        return balance;
    }
    @Override
    public String toString() {
        
        //Định dạng tiền
        Locale usLocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);

        return customer.getName().toString() + "(" + id + "), balance=" + usNumberFormat.format(balance) + "]";
    }
}
