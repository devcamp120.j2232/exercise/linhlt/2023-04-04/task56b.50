package com.devcamp.customeraccountapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customeraccountapi.Account;
import com.devcamp.customeraccountapi.Customer;

@RestController
@RequestMapping("/")
@CrossOrigin
public class AccountControllers {
    @GetMapping("/accounts")
    public ArrayList<Account> getAccountApi(){
        //task 4
        Customer customer1 = new Customer(1, "Linh", 20);
        Customer customer2 = new Customer(2, "Minh", 10);
        Customer customer3 = new Customer(2, "Trang", 20);
        System.out.println(customer1);
        System.out.println(customer2);
        System.out.println(customer3);
        //task 5
        Account account1 = new Account(1001, customer1, 10000000);
        Account account2 = new Account(1002, customer2, 200000000);
        Account account3 = new Account(1003, customer3, 50000000);
        System.out.println(account1);
        System.out.println(account2);
        System.out.println(account3);

        //task 6
        ArrayList<Account> accountList = new ArrayList<>();
        accountList.add(account1);
        accountList.add(account2);
        accountList.add(account3);
        return accountList;
    }
    
}
